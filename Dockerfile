# original docker image playwright
FROM mcr.microsoft.com/playwright:v1.42.1-jammy

WORKDIR /app

# Copy dependencies and init.sh script to '/app' inside docker
COPY ./package.json ./package-lock.json ./docker/init.sh ./

RUN apt-get update && apt-get install -y xvfb \
    && npm install \
    && npm run init-playwright \
    && chmod u+x ./init.sh

COPY . .

CMD /bin/bash -c './init.sh'