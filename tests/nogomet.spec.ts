import { test, expect } from '@playwright/test';

test('login', async ({ page }) => {
  await page.goto('https://nogomet.onrender.com/');
  await page.getByPlaceholder('Enter e-mail address').click();
  await page.getByPlaceholder('Enter e-mail address').fill('nik.perne@gmail.com');
  await page.getByPlaceholder('Enter password').click();
  await page.getByPlaceholder('Enter password').fill('Oklop123');
  await page.getByRole('button', { name: ' Login' }).click();
  const textEvent = page.locator('a').filter({ hasText: 'Add Event' })
  await expect(textEvent).toBeVisible();
});

test('logout', async ({ page }) => {
  await login(page);
  await page.getByRole('button', { name: ' Nik Perne' }).click();
  await page.getByText('Logout').click();
  const textSignIn = page.getByRole('heading', { name: 'Sign in' });
  await expect(textSignIn).toBeVisible();
});

test('false_user', async ({ page }) => {
  await page.goto('https://nogomet.onrender.com/');
  await page.fill('input[name="email"]', 'blabla@gmail.com');
  await page.fill('input[name="password"]', 'sdsfsdfsdf');
  await page.getByRole('button', { name: ' Login' }).click();
  await expect(page.locator('text=Incorrect username.')).toBeVisible();
});

test('empty_login', async ({ page }) => {
  await page.goto('https://nogomet.onrender.com/');
  await page.fill('input[name="email"]', '');
  await page.fill('input[name="password"]', '');
  await page.getByRole('button', { name: ' Login' }).click();
  await expect(page.locator('text=All fields are required, please try again.')).toBeVisible();
});

test('no_mail_user', async ({ page }) => {
  await page.goto('https://nogomet.onrender.com/');
  await page.fill('input[name="email"]', 'blabla');
  await page.fill('input[name="password"]', 'sdgfsgsdgs');
  await page.getByRole('button', { name: ' Login' }).click();
  await expect(page.locator('text=Please enter a valid e-mail')).toBeVisible();
});

test('wrong_password', async ({ page }) => {
  await page.goto('https://nogomet.onrender.com/');
  await page.fill('input[name="email"]', 'nik.perne@gmail.com');
  await page.fill('input[name="password"]', 'adsfghsadfhsa');
  await page.getByRole('button', { name: ' Login' }).click();
  await expect(page.locator('text=Incorrect password.')).toBeVisible();
});

test('register_existing_user', async ({ page }) => {
  await page.goto('https://nogomet.onrender.com/');
  await page.getByRole('link', { name: 'register' }).click();
  await page.getByPlaceholder('Enter your name').click();
  await page.getByPlaceholder('Enter your name').fill('Nik Perne');
  await page.getByPlaceholder('Enter e-mail address').click();
  await page.getByPlaceholder('Enter e-mail address').fill('nik.perne@gmail.com');
  await page.getByPlaceholder('Enter password').click();
  await page.getByPlaceholder('Enter password').fill('dfsdfsdfsd');
  await page.getByRole('button', { name: ' Register' }).click();
  await expect(page.locator('text=User with given e-mail')).toBeVisible();
});

test('register_user_wrong_mail', async ({ page }) => {
  await page.goto('https://nogomet.onrender.com/');
  await page.getByRole('link', { name: 'register' }).click();
  await page.getByPlaceholder('Enter your name').click();
  await page.getByPlaceholder('Enter your name').fill('Nik Perne');
  await page.getByPlaceholder('Enter e-mail address').click();
  await page.getByPlaceholder('Enter e-mail address').fill('nik.perne');
  await page.getByPlaceholder('Enter password').click();
  await page.getByPlaceholder('Enter password').fill('dfsdfsdfsd');
  await page.getByRole('button', { name: ' Register' }).click();
  await expect(page.locator('text=Please enter a valid e-mail address.')).toBeVisible();
});

test('register_missing_field', async ({ page }) => {
  await page.goto('https://nogomet.onrender.com/');
  await page.getByRole('link', { name: 'register' }).click();
  await page.getByPlaceholder('Enter your name').click();
  await page.getByPlaceholder('Enter your name').fill('Nik Perne');
  await page.getByPlaceholder('Enter e-mail address').click();
  await page.getByPlaceholder('Enter e-mail address').fill('nik.perne');
  await page.getByPlaceholder('Enter password').click();
  await page.getByPlaceholder('Enter password').fill('dfsdfsdfsd');
  await page.getByRole('button', { name: ' Register' }).click();
  await expect(page.locator('text=Please enter a valid e-mail address.')).toBeVisible();
});

test('odjava_prijava', async ({ page }) => {
  await login(page);
  await page.click('text=20. februar,');
  await page.click('text=Nik Perne');
  const button = page.getByRole('button', { name: ' Pridem' });;
  const text = page.locator('app-event-details').getByText('Nik Perne');
  await expect(text).toBeVisible();
});

test('button_disabled_check', async ({ page }) => {
  await login(page);
  await page.click('text=20. februar,');
  const button = page.getByRole('button', { name: ' Pridem' });
  await expect(button).toBeDisabled();
});

test('about_page', async ({ page }) => {
  await login(page);
  await page.getByRole('button', { name: 'Information' }).click();
  await page.getByRole('link', { name: 'O aplikaciji' }).click();
  const welcome = page.getByText('Dobrodošli v aplikaciji');
  await expect(welcome).toBeVisible();
});

async function login(page) {
  await page.goto('https://nogomet.onrender.com/');
  await page.getByPlaceholder('Enter e-mail address').click();
  await page.getByPlaceholder('Enter e-mail address').fill('nik.perne@gmail.com');
  await page.getByPlaceholder('Enter password').click();
  await page.getByPlaceholder('Enter password').fill('Oklop123');
  await page.getByRole('button', { name: ' Login' }).click();
  const textEvent = page.locator('a').filter({ hasText: 'Add Event' })
  await expect(textEvent).toBeVisible();
}
